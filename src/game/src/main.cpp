#include <prd/api.hpp>

int main()
{
  dice::game     g;
  dice::creature c{"coucou"};

  {
    dice::engine::linker l;

    l.link(prd::Character, c);

    {
      l.link(prd::races::Aasimar, c);
      assert(c.charisma() == 2);
    }
  }

  assert(c.charisma() == 0);

  {
    dice::creature       c{"coucou2"};
    dice::engine::linker l;

    l.link(prd::Character, c);
    l.link(prd::races::Dwarf, c);
    l.link(prd::scores::mean_ability_scores, c);

    auto                                range = c.charisma.all();
    std::vector<dice::engine::modifier> mods{range.begin(), range.end()};

    assert(mods.size() == 2);

    assert(mods[0].type_ == dice::engine::modifier::type::racial_bonus);
    assert(mods[0].source_ == prd::races::Dwarf.id());
    assert(mods[0].value_ == -2);

    assert(mods[1].type_ == dice::engine::modifier::type::base_value);
    assert(mods[1].source_ == prd::scores::mean_ability_scores.id());
    assert(mods[1].value_ == 9);

    assert(c.charisma() == 7);

    auto p = std::unique_ptr<int>{new int(1)};
  }

  return 0;
}
