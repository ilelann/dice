// no preprocessor allowed here
// this file is loaded by luajit

typedef struct d20script_object {
	const char * type;
} d20script_object_t;

d20script_object_t * d20script_create_game();
void d20script_close(d20script_object_t * o);

void d20script_link(d20script_object_t * a, d20script_object_t * b);

d20script_object_t * d20script_create_ability_scores(d20script_object_t * root);
d20script_object_t * d20script_create_standard_ability_scores(d20script_object_t * root);
d20script_object_t * d20script_create_classic_ability_scores(d20script_object_t * root);
d20script_object_t * d20script_create_heroic_ability_scores(d20script_object_t * root);

d20script_object_t * d20script_create_creature(d20script_object_t * root, const char * name);

d20script_object_t * d20script_create_constitution_to_hit_die(d20script_object_t * root);
d20script_object_t * d20script_create_constitution_to_fortitude_saving_throw(d20script_object_t * root);
d20script_object_t * d20script_create_dexterity_to_ranged_attack_roll(d20script_object_t * root);
d20script_object_t * d20script_create_dexterity_to_armor_class(d20script_object_t * root);
d20script_object_t * d20script_create_dexterity_to_reflex_saving_throw(d20script_object_t * root);
d20script_object_t * d20script_create_strength_to_melee_attack_roll(d20script_object_t * root);
d20script_object_t * d20script_create_strength_to_melee_damage_roll(d20script_object_t * root);
d20script_object_t * d20script_create_wisdom_to_will_saving_throw(d20script_object_t * root);

d20script_object_t * d20script_create_score_bonus(d20script_object_t * root, unsigned int value);


typedef struct d20script_iterator d20script_iterator_t;

void d20script_array_delete(d20script_iterator_t *);
d20script_object_t * d20script_array_next (d20script_iterator_t *);
d20script_object_t * d20script_array_at (d20script_iterator_t * a, unsigned int index);
unsigned int d20script_array_size (d20script_iterator_t *);

d20script_iterator_t * d20script_game_creatures(d20script_object_t *);


typedef struct d20script_str {
	const char * data;
	unsigned long length;
} d20script_str_t;

d20script_str_t d20script_creature_name(d20script_object_t *);

typedef d20script_object_t d20script_score_t;

unsigned int d20script_score_value(d20script_score_t *);

d20script_score_t * d20script_creature_strength(d20script_object_t *);
d20script_score_t * d20script_creature_dexterity(d20script_object_t *);
d20script_score_t * d20script_creature_constitution(d20script_object_t *);
d20script_score_t * d20script_creature_intelligence(d20script_object_t *);
d20script_score_t * d20script_creature_wisdom(d20script_object_t *);
d20script_score_t * d20script_creature_charisma(d20script_object_t *);

d20script_score_t * d20script_creature_melee_attack_roll(d20script_object_t *);
d20script_score_t * d20script_creature_ranged_attack_roll(d20script_object_t *);
d20script_score_t * d20script_creature_melee_damage_roll(d20script_object_t *);
d20script_score_t * d20script_creature_fortitude_saving_throw(d20script_object_t *);
d20script_score_t * d20script_creature_reflex_saving_throw(d20script_object_t *);
d20script_score_t * d20script_creature_will_saving_throw(d20script_object_t *);
d20script_score_t * d20script_creature_hit_die(d20script_object_t *);
d20script_score_t * d20script_creature_max_load(d20script_object_t *);
d20script_score_t * d20script_creature_armor_class(d20script_object_t *);
d20script_score_t * d20script_creature_touched_armor_class(d20script_object_t *);
d20script_score_t * d20script_creature_speed(d20script_object_t *);
