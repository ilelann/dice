#include <d20script/api.h>

#include <dice/api.hpp>

#include <memory>
#include <vector>

namespace
{
  struct object_base : d20script_object
  {
    virtual ~object_base()
    {
    }
  };

  template <typename T>
  struct object : T, object_base
  {
    template <typename... Args>
    object(Args&&... args) : T(std::forward<Args>(args)...)
    {
    }
  };
}

struct score : object_base
{
  score(dice::engine::score& score) : score_m(&score)
  {
    type = "score";
  }

  dice::engine::score* score_m;
};

namespace
{
  template <typename T, typename U>
  T& as(U* u)
  {
    return dynamic_cast<T&>(static_cast<object_base&>(*u));
  }

  template <typename U>
  dice::engine::connected& as_connected(U* u)
  {
    auto& ob = static_cast<object_base&>(*u);

    if (auto s = dynamic_cast<score*>(&ob)) {
      return *(s->score_m);
    }

    return dynamic_cast<dice::engine::connected&>(ob);
  }

  struct root : object<dice::game>
  {
    root()
    {
      type = "game";
    }

    template <typename T, typename... Args>
    d20script_object_t* add(const char* type, Args&&... args)
    {
      _objects.push_back(create_object<T>(type, std::forward<Args>(args)...));
      return _objects.back().get();
    }

  private:
    typedef std::unique_ptr<object_base> pointer_type;

    template <typename T, typename... Args>
    pointer_type create_object(const char* type, Args&&... args)
    {
      auto res = std::unique_ptr<object<T>>{new object<T>(std::forward<Args>(args)...)};

      res->type = type;

      return std::move(res);
    }

    std::vector<pointer_type> _objects;
  };

  template <typename T, typename... Args>
  d20script_object_t* add(d20script_object_t* r, const char* type, Args&&... args)
  {
    return as<root>(r).add<T>(type, std::forward<Args>(args)...);
  }
}

extern "C" {
void d20script_link(d20script_object_t* a, d20script_object_t* b)
{
  link(as_connected(a), as_connected(b));
}

void d20script_close(d20script_object_t* o)
{
  delete static_cast<object_base*>(o);
}

d20script_object_t* d20script_create_game()
{
  return new root();
}

d20script_object_t* d20script_create_creature(d20script_object_t* root, const char* name)
{
  return add<dice::creature>(root, "creature", name);
}

#define D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE2__(name, ...)                                        \
  d20script_object_t* d20script_create_##name(d20script_object_t* root)                            \
  {                                                                                                \
    return add<dice::__VA_ARGS__>(root, #__VA_ARGS__);                                             \
  }

#define D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE2_(name, ...)                                         \
  D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE2__(name, __VA_ARGS__)

#define D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_(o) D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE2_(o, o)

#define D20SCRIPT_API_MODIFIER_NAME__(src, dst) src##_to_##dst

#define D20SCRIPT_API_MODIFIER_NAME_(src, dst) D20SCRIPT_API_MODIFIER_NAME__(src, dst)

#define dice_OBJECTS_BASICS_MODIFIER(src, dst)                                                     \
  ability_modifier<&dice::creature::src, &dice::creature::dst>

#define D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_MODIFIER_(src, dst)                                  \
  D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE2_(D20SCRIPT_API_MODIFIER_NAME_(src, dst),                  \
                                          dice_OBJECTS_BASICS_MODIFIER(src, dst))

D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_(ability_scores)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_(standard_ability_scores)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_(classic_ability_scores)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_(heroic_ability_scores)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_MODIFIER_(constitution, hit_die)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_MODIFIER_(constitution, fortitude_saving_throw)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_MODIFIER_(dexterity, ranged_attack_roll)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_MODIFIER_(dexterity, armor_class)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_MODIFIER_(dexterity, reflex_saving_throw)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_MODIFIER_(strength, melee_attack_roll)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_MODIFIER_(strength, melee_damage_roll)
D20SCRIPT_API_DEFINE_D20SCRIPT_CREATE_MODIFIER_(wisdom, will_saving_throw)

d20script_object_t* d20script_create_score_bonus(d20script_object_t* root, unsigned int value)
{
  return add<dice::score_bonus>(root, "score_bonus", value, dice::engine::modifier::type::none);
}
}

extern "C" {
typedef struct d20script_iterator
{
} d20script_iterator_t;
}

namespace
{
  struct iterator_base : d20script_iterator_t
  {
    virtual object_base* next() = 0;

    virtual object_base* at(unsigned int offset) = 0;

    virtual unsigned int size() = 0;

    virtual ~iterator_base()
    {
    }
  };

  template <typename Range>
  struct range_iterator : iterator_base
  {
    range_iterator(const Range& arg) : range(arg), position(arg.begin())
    {
    }

    object_base* next()
    {
      if (position == range.end()) {
        return nullptr;
      }

      auto res = &*position;

      ++position;

      return const_cast<object_base*>(dynamic_cast<const object_base*>(res));
    }

    object_base* at(unsigned int offset)
    {
      auto res = position;
      std::advance(res, offset);

      return const_cast<object_base*>(dynamic_cast<const object_base*>(&*res));
    }

    unsigned int size()
    {
      return std::distance(position, std::end(range));
    }

  private:
    const Range&                   range;
    typename Range::const_iterator position;
  };

  template <typename Range>
  iterator_base* make_range_iterator(const Range& range)
  {
    return new range_iterator<Range>(range);
  }
}

extern "C" {
void d20script_array_delete(d20script_iterator_t* a)
{
  delete static_cast<iterator_base*>(a);
}

d20script_object_t* d20script_array_next(d20script_iterator_t* a)
{
  return (static_cast<iterator_base*>(a))->next();
}

d20script_object_t* d20script_array_at(d20script_iterator_t* a, unsigned int index)
{
  return (static_cast<iterator_base*>(a))->at(index);
}

unsigned int d20script_array_size(d20script_iterator_t* a)
{
  return (static_cast<iterator_base*>(a))->size();
}

d20script_iterator_t* d20script_game_creatures(d20script_object_t* o)
{
  return make_range_iterator(as<dice::game>(o).creatures);
}

d20script_str_t d20script_creature_name(d20script_object_t* o)
{
  const auto& res = as<dice::creature>(o).name;

  return {res.c_str(), res.size()};
}

unsigned int d20script_score_value(d20script_score_t* s)
{
  return (*(as<score>(s).score_m))();
}

d20script_score_t* d20script_creature_strength(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).strength);
}

d20script_score_t* d20script_creature_dexterity(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).dexterity);
}

d20script_score_t* d20script_creature_constitution(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).constitution);
}

d20script_score_t* d20script_creature_intelligence(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).intelligence);
}

d20script_score_t* d20script_creature_wisdom(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).wisdom);
}

d20script_score_t* d20script_creature_charisma(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).charisma);
}

d20script_score_t* d20script_creature_melee_attack_roll(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).melee_attack_roll);
}

d20script_score_t* d20script_creature_ranged_attack_roll(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).ranged_attack_roll);
}

d20script_score_t* d20script_creature_melee_damage_roll(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).melee_damage_roll);
}

d20script_score_t* d20script_creature_fortitude_saving_throw(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).fortitude_saving_throw);
}

d20script_score_t* d20script_creature_reflex_saving_throw(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).reflex_saving_throw);
}

d20script_score_t* d20script_creature_will_saving_throw(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).will_saving_throw);
}

d20script_score_t* d20script_creature_hit_die(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).hit_die);
}

d20script_score_t* d20script_creature_max_load(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).max_load);
}

d20script_score_t* d20script_creature_armor_class(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).armor_class);
}

d20script_score_t* d20script_creature_touched_armor_class(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).touched_armor_class);
}

d20script_score_t* d20script_creature_speed(d20script_object_t* o)
{
  return new score(as<dice::creature>(o).speed);
}
}
