#include <bandit/bandit.h>

#include <d20script/api.h>

using namespace bandit;

// NB : these tests leaks as hell

go_bandit([] {
  describe("d20script's", [] {
    describe("link", [] {
      auto game     = d20script_create_game();
      auto creature = d20script_create_creature(game, "name");

      it("should work on object base", [=] {
        AssertThat(d20script_array_size(d20script_game_creatures(game)), Equals(0u));

        d20script_link(game, creature);

        AssertThat(d20script_array_size(d20script_game_creatures(game)), Equals(1u));
      });

      it("should work creature", [=] {
        AssertThat(d20script_score_value(d20script_creature_strength(creature)), Equals(0u));

        d20script_link(creature, d20script_create_ability_scores(game));

        AssertThat(d20script_score_value(d20script_creature_strength(creature)), Equals(9u));
      });

      it("should work creature's score", [=] {
        AssertThat(d20script_score_value(d20script_creature_strength(creature)), Equals(9u));

        d20script_link(d20script_creature_strength(creature),
                       d20script_create_score_bonus(game, 3));

        AssertThat(d20script_score_value(d20script_creature_strength(creature)), Equals(12u));
      });
    });
  });
});

int main(int argc, char* argv[])
{
  return bandit::run(argc, argv);
}
