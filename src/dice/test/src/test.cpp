#include <bandit/bandit.h>

#include <dice/api.hpp>

#include <numeric>

using namespace bandit;
using namespace dice;

go_bandit([] {
  describe("dice's", [] {
    describe("callback", [] {
      it("should accept new slot", [] {
        auto c = engine::slots<int()>{};

        auto con = c.connect_value_(3);
        AssertThat(std::distance(c.all().begin(), c.all().end()), Equals(1));
        AssertThat(std::accumulate(c.all().begin(), c.all().end(), 0), Equals(3));

        // connection not stored : no-op
        c.connect_value_(2);
        AssertThat(std::distance(c.all().begin(), c.all().end()), Equals(1));
        AssertThat(std::accumulate(c.all().begin(), c.all().end(), 0), Equals(3));

        {
          auto con = c.connect_value_(4);
          AssertThat(std::distance(c.all().begin(), c.all().end()), Equals(2));
          AssertThat(std::accumulate(c.all().begin(), c.all().end(), 0), Equals(7));
        }

        AssertThat(std::distance(c.all().begin(), c.all().end()), Equals(1));
        AssertThat(std::accumulate(c.all().begin(), c.all().end(), 0), Equals(3));

        {
          auto con = c.connect_([] { return 7; });

          AssertThat(std::distance(c.all().begin(), c.all().end()), Equals(2));
          AssertThat(std::accumulate(c.all().begin(), c.all().end(), 0), Equals(10));
        }
      });
    });

    describe("modifier_combiner", [] {
      it("should combine modifiers", [] {
        engine::modifier_combiner c{0};

        std::list<engine::modifier> inputs;
        AssertThat(c(begin(inputs), end(inputs)), Equals(0));

        inputs.emplace_back(4, engine::modifier::type::base_value, 1);
        AssertThat(c(begin(inputs), end(inputs)), Equals(4));

        inputs.emplace_back(2, engine::modifier::type::shield_bonus, 2);
        AssertThat(c(begin(inputs), end(inputs)), Equals(6));

        it("using max by type for regular modifiers", [&] {
          // only max of bonuses apply
          inputs.emplace_back(3, engine::modifier::type::shield_bonus, 3);
          AssertThat(c(begin(inputs), end(inputs)), Equals(7));
        });

        it("using max by source for none", [&] {
          // none special case ...
          inputs.emplace_back(4, engine::modifier::type::none, 4);
          AssertThat(c(begin(inputs), end(inputs)), Equals(11));

          // stacks...
          inputs.emplace_back(3, engine::modifier::type::none, 5);
          AssertThat(c(begin(inputs), end(inputs)), Equals(14));

          // ...unless same source
          inputs.emplace_back(1, engine::modifier::type::none, 5);
          AssertThat(c(begin(inputs), end(inputs)), Equals(14));

          // in which case we take max
          inputs.emplace_back(12, engine::modifier::type::none, 5);
          AssertThat(c(begin(inputs), end(inputs)), Equals(23));
        });

        it("using all for dodge and circumstance", [&] {
          // none special case ...
          inputs.emplace_back(4, engine::modifier::type::dodge_bonus, 4);
          inputs.emplace_back(5, engine::modifier::type::dodge_bonus, 4);
          inputs.emplace_back(6, engine::modifier::type::circumstance_modifier, 4);
          inputs.emplace_back(7, engine::modifier::type::circumstance_modifier, 4);
          AssertThat(c(begin(inputs), end(inputs)), Equals(45));
        });
      });
    });

    describe("score", [] {
      it("should combine modifiers", [] {
        engine::score s;
        AssertThat(s(), Equals(0));

        auto con1 = s.connect_value_(engine::modifier{4, engine::modifier::type::base_value, 1});
        AssertThat(s(), Equals(4));

        auto con2 =
            s.connect_value_(engine::modifier{2, engine::modifier::type::ability_modifier, 2});
        AssertThat(s(), Equals(6));
      });
    });

    describe("ability", [] {
      it("should not go below it's min value", [] { AssertThat(engine::ability{1}(), Equals(1)); });
    });

    describe("ability_modifier", [] {
      it("should handle all values of source ability", [] {
        AssertThat(make_mod(engine::ability{1}, 0)().value_, Equals(-5));
        AssertThat(make_mod(engine::ability{2}, 0)().value_, Equals(-4));
        AssertThat(make_mod(engine::ability{3}, 0)().value_, Equals(-4));
        AssertThat(make_mod(engine::ability{4}, 0)().value_, Equals(-3));
        AssertThat(make_mod(engine::ability{5}, 0)().value_, Equals(-3));
        AssertThat(make_mod(engine::ability{9}, 0)().value_, Equals(-1));
        AssertThat(make_mod(engine::ability{10}, 0)().value_, Equals(0));
        AssertThat(make_mod(engine::ability{11}, 0)().value_, Equals(0));
        AssertThat(make_mod(engine::ability{14}, 0)().value_, Equals(2));
        AssertThat(make_mod(engine::ability{32}, 0)().value_, Equals(11));
      });
    });

    describe("object", [] {
      it("should destruct cleanly by pointer", [] {
        creature c{"test"};
        {
          engine::linker l;
          auto           s = dice::make_ability_scores([] { return 9; });

          l.link(s, c);
          AssertThat(c.strength(), Equals(c.strength()));
          AssertThat(c.strength(), Equals(9));
        }
        AssertThat(c.strength(), Equals(c.strength()));
        AssertThat(c.strength(), Equals(0));
      });
      it("should destruct in any order", [] {
        {
          creature c{"test"};
          {
            engine::linker l;
            auto           s = dice::make_ability_scores([] { return 9; });
            l.link(s, c);
            AssertThat(c.strength(), Equals(9));
          }
          AssertThat(c.strength(), Equals(0));
        }
        {
          auto s = dice::make_ability_scores([] { return 9; });
          {
            engine::linker l;
            creature       c{"test"};
            AssertThat(c.strength(), Equals(0));

            l.link(s, c);
            AssertThat(c.strength(), Equals(9));
          }
        }
      });
    });

    describe("creature", [] {
      it("should have abilities", [] {
        creature c{"test"};

        AssertThat(c.strength(), Equals(0));

        {
          auto           scores = dice::make_ability_scores(dice::sum4d6b3);
          engine::linker l;
          l.link(scores, c);
          AssertThat(c.strength(), Equals(c.strength()));
          AssertThat(c.strength(), IsGreaterThan(2));
          AssertThat(c.strength(), IsLessThan(19));
        }

        AssertThat(c.strength(), Equals(0));

        {
          auto           scores = dice::make_ability_scores([] { return 9; });
          engine::linker l;
          l.link(scores, c);
          AssertThat(c.strength(), Equals(c.strength()));
          AssertThat(c.strength(), Equals(9));
          AssertThat(c.dexterity(), Equals(9));
        }

        AssertThat(c.strength(), Equals(0));

        {
          auto           scores = dice::make_ability_scores(dice::sum3d6);
          engine::linker l;
          l.link(scores, c);
          AssertThat(c.constitution(), Equals(c.constitution()));
          AssertThat(c.constitution(), IsGreaterThan(2));
          AssertThat(c.constitution(), IsLessThan(19));
        }

        AssertThat(c.strength(), Equals(0));

        {
          engine::linker l;
          auto           scores = dice::make_ability_scores([] { return dice::sum2d6() + 6; });
          l.link(scores, c);
          AssertThat(c.wisdom(), Equals(c.wisdom()));
          AssertThat(c.wisdom(), IsGreaterThan(7));
          AssertThat(c.wisdom(), IsLessThan(19));
        }

        AssertThat(c.strength(), Equals(0));
      });

      it("should have modifiers", [] {
        engine::linker l;
        creature       c{"test"};
        auto           scores = dice::make_ability_scores([] { return 9; });
        l.link(scores, c);

        score_bonus bonus{3, engine::modifier::type::none};
        l.link(bonus, c.strength);

        AssertThat(c.strength(), Equals(12));
        AssertThat(c.melee_attack_roll(), Equals(0));

        ability_modifier<&creature::strength, &creature::melee_attack_roll> modifier;
        l.link(modifier, c);

        AssertThat(c.melee_attack_roll(), Equals(1));
      });
    });
  });
});

int main(int argc, char* argv[])
{
  return bandit::run(argc, argv);
}
