#pragma once

#include <dice/config.hpp>

namespace dice
{
  struct creature;

  namespace engine
  {
    struct linker;
  }

  struct DICE_API max_load_from_strength
  {
    void attach_to(creature& p, engine::linker& l) const;
  };
}
