#pragma once

#include <dice/config.hpp>
#include <dice/engine/connection.hpp>

#include <vector>

namespace dice
{
  namespace engine
  {
    struct linker
    {
      template <typename From, typename To>
      void link(const From& from, To& to)
      {
        from.attach_to(to, *this);
      }

      template <typename Signal, typename Func>
      void connect(Signal& s, Func&& f) const
      {
        adopt(s.connect_(std::forward<Func>(f)));
      }

      template <typename Signal, typename... Args>
      void connect_value(Signal& s, Args&&... args) const
      {
        adopt(s.connect_value_(std::forward<Args>(args)...));
      }

    private:
      void adopt(connection con) const
      {
        m_connections.push_back(std::move(con));
      }

      mutable std::vector<engine::connection> m_connections;
    };
  }
}
