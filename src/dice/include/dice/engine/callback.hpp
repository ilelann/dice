#pragma once

#include <dice/config.hpp>
#include <dice/engine/connection.hpp>

#include <boost/iterator/iterator_adaptor.hpp>

#include <functional>
#include <list>
#include <memory>

namespace dice
{
  namespace engine
  {
    auto slot_invoker = [](auto... args) {
      return [args...](auto f) -> decltype(auto) { return f(args...); };
    };

    template <typename Base, typename slot_invoker_type>
    struct slot_call_iterator : boost::iterator_adaptor<slot_call_iterator<Base, slot_invoker_type>,
                                                        Base,
                                                        typename Base::value_type::result_type,
                                                        boost::iterators::use_default,
                                                        typename Base::value_type::result_type>
    {
      explicit slot_call_iterator(const Base& first, slot_invoker_type* slot_invoker)
          : slot_call_iterator::iterator_adaptor_{first}, slot_invoker_{slot_invoker}
      {
      }

      auto dereference() const
      {
        return (*slot_invoker_)(*(slot_call_iterator::iterator_adaptor_::base()));
      }

      slot_invoker_type* slot_invoker_;
    };

    template <typename Invoker, typename Iterator>
    struct slot_call_range
    {
      slot_call_range(Invoker invoker, Iterator begin, Iterator end)
          : invoker_{invoker}, begin_{begin}, end_{end}
      {
      }

      auto begin()
      {
        return slot_call_iterator{begin_, &invoker_};
      }

      auto end()
      {
        return slot_call_iterator{end_, (Invoker*)nullptr};
      }

    private:
      Invoker  invoker_;
      Iterator begin_;
      Iterator end_;
    };

    template <typename Signature>
    struct slots;

    template <typename R, typename... Args>
    struct slots<R(Args...)>
    {
      using slot_type        = std::function<R(Args...)>;
      using slot_result_type = typename slot_type::result_type;
      using container_type   = std::list<slot_type>;

      auto all(Args... args) const
      {
        return slot_call_range{slot_invoker(args...), container().cbegin(), container().cend()};
      }

      template <typename S>
      connection connect_(S&& s)
      {
        auto iter = container().emplace(container().end(), std::forward<S>(s));
        auto weak = std::weak_ptr<container_type>{container_};

        auto cleanup = [iter, weak] {
          if (auto shared = weak.lock()) {
            shared->erase(iter);
          }
        };

        return connection{cleanup};
      }

      template <typename R2 = slot_result_type>
      connection connect_value_(slot_result_type v,
                                std::enable_if_t<std::is_reference_v<R2>, connection>* = nullptr)
      {
        return connect_([&v](Args...) { return v; });
      }

      template <typename R2 = slot_result_type>
      connection connect_value_(slot_result_type v,
                                std::enable_if_t<!std::is_reference_v<R2>, connection>* = nullptr)
      {
        return connect_([v](Args...) { return v; });
      }

    private:
      container_type& container()
      {
        return *container_;
      }

      const container_type& container() const
      {
        return *container_;
      }

      std::shared_ptr<container_type> container_{new container_type};
    };

    template <typename Signature, typename Combiner>
    struct combined_slots : slots<Signature>
    {
      combined_slots()
      {
      }

      combined_slots(Combiner combiner) : combiner_{combiner}
      {
      }

      template <typename... Args>
      auto operator()(Args&&... args) const
      {
        auto range = this->all(std::forward<Args>(args)...);
        return combiner_(range.begin(), range.end());
      }

    private:
      Combiner combiner_;
    };

    template <typename signature, typename combiner>
    using callback = combined_slots<signature, combiner>;

    template <typename T, T default_value>
    struct last_combiner
    {
      template <typename Iterator>
      T operator()(Iterator first, Iterator last) const
      {
        return (first != last) ? *(std::prev(last)) : default_value;
      }
    };

    template <typename result_type, result_type default_value>
    using last_one = combined_slots<result_type(), last_combiner<result_type, default_value>>;
  }
}
