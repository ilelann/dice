#pragma once

#include <dice/config.hpp>
#include <dice/engine/modifier.hpp>
#include <dice/engine/modifier_source.hpp>

#include <map>

namespace dice
{
  namespace engine
  {
    struct modifier_combiner
    {
      using result_type = modifier::result_type;

      modifier_combiner(const result_type& min_final_value) : min_final_value_{min_final_value}
      {
      }

      template <typename InputIterator>
      result_type operator()(InputIterator first, InputIterator last) const
      {
        // to handle default modifiers using max by type
        std::map<modifier::type, result_type> type_map;
        // to handle "none"-typed modifiers using max by source
        std::map<modifier_source::id_type, result_type> source_map;

        result_type ret{};

        while (first != last) {
          const modifier&    received = *first;
          const result_type& value    = received.value_;
          if (takes_all(received)) {
            ret += value;
          }
          else if (takes_max_by_source(received)) {
            ret += insert_or_update_max(source_map, received.source_, value);
          }
          else {
            ret += insert_or_update_max(type_map, received.type_, value);
          }
          ++first;
        }
        return std::max(ret, min_final_value_);
      }

    private:
      template <typename Map>
      typename Map::mapped_type insert_or_update_max(Map&                             m,
                                                     const typename Map::key_type&    k,
                                                     const typename Map::mapped_type& v) const
      {
        auto ib = m.emplace(k, v);
        if (ib.second) {
          return v;
        }
        else {
          auto& old_v = ib.first->second;
          if (old_v < v) {
            auto res = v - old_v;
            old_v    = v;
            return res;
          }
          return 0;
        }
      }

      bool takes_all(const modifier& m) const
      {
        return m.type_ == modifier::type::dodge_bonus
               || m.type_ == modifier::type::circumstance_modifier;
      }

      bool takes_max_by_source(const modifier& m) const
      {
        return m.type_ == modifier::type::none;
      }

      const result_type min_final_value_;
    };
  }
}
