#pragma once

#include <functional>

namespace dice
{
  namespace engine
  {

    struct connection
    {
      using deleter_type = std::function<void()>;

      explicit connection(deleter_type deleter) : deleter_(deleter)
      {
      }

      connection(connection&&) = default;
      connection& operator=(connection&&) = default;

      connection(const connection&) = delete;
      connection& operator=(const connection&) = delete;

      ~connection()
      {
        if (deleter_) {
          deleter_();
        }
      }

    private:
      deleter_type deleter_;
    };
  }
}
