#pragma once

#include <dice/config.hpp>

namespace dice
{
  namespace engine
  {
    struct DICE_API identified
    {
      identified();

      using id_type = uinteger;

      id_type id() const
      {
        return m_object_id;
      }

      template <typename T>
      static auto value()
      {
        static auto id = ++counter;

        return id;
      }

    protected:
      const id_type m_object_id;

    private:
      static id_type counter;
    };
  }
}
