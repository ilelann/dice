#pragma once

#include <dice/config.hpp>
#include <dice/engine/score.hpp>

namespace dice
{
  namespace engine
  {
    using ability = score;
  }
}
