#pragma once

#include <dice/config.hpp>

#include <dice/engine/linker.hpp>

#include <assert.h>

namespace dice
{
  namespace engine
  {
    template <typename Self, typename other, typename... components>
    struct compound_object_of
    {
      static auto id()
      {
        return identified::value<Self>();
      }

      void attach_to(other& p, linker& l) const
      {
        [](...) {}((components::template attach_to<Self>(p, l), 0)...);
      }
    };
  }
}
