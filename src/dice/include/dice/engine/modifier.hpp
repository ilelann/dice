#pragma once

#include <dice/config.hpp>
#include <dice/engine/modifier_source.hpp>

namespace dice
{
  namespace engine
  {
    struct modifier
    {
      using result_type = integer;

      enum class type
      {
        none,
        ability_modifier,
        alchemical_bonus,
        armor_bonus,
        circumstance_modifier,
        competence_modifier,
        deflection_bonus,
        dodge_bonus,
        enhancement_bonus,
        insight_bonus,
        luck_modifier,
        morale_modifier,
        natural_armor_bonus,
        profane_modifier,
        racial_bonus,
        resistance_bonus,
        sacred_modifier,
        shield_bonus,
        size_modifier,
        // "base_value" is implementation-related
        // it allows to have base value treated like a modifier
        // ex : abilities rolled initial value, 10 base for armor class, etc
        base_value
      };

      modifier(const result_type& value, const type& t, const modifier_source::id_type& source)
          : value_{value}, type_{t}, source_{source}
      {
      }

      result_type              value_;
      type                     type_;
      modifier_source::id_type source_;
    };
  }
}
