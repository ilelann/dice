#pragma once

#include <dice/config.hpp>
#include <dice/engine/identified.hpp>

namespace dice
{
  namespace engine
  {
    using modifier_source = identified;
  }
}
