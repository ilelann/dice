#pragma once

#include <dice/config.hpp>
#include <dice/engine/callback.hpp>
#include <dice/engine/modifier.hpp>
#include <dice/engine/modifier_combiner.hpp>

#include <limits>

namespace dice
{
  namespace engine
  {
    struct score : callback<modifier(), modifier_combiner>
    {
      using result_type   = modifier::result_type;
      using callback_type = callback<modifier(), modifier_combiner>;

      score(const result_type& min_final_value = std::numeric_limits<result_type>::min())
          : callback_type{modifier_combiner{min_final_value}}
      {
      }
    };
  }
}
