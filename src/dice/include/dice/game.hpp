#pragma once

#include <dice/config.hpp>
#include <dice/engine/callback.hpp>

#include <map>

namespace dice
{
  struct creature;

  struct DICE_API game
  {
    engine::slots<const creature&()> creatures;
  };
}
