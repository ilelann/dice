#pragma once

#include <dice/config.hpp>
#include <dice/engine/ability.hpp>
#include <dice/engine/linker.hpp>
#include <dice/engine/modifier_source.hpp>
#include <dice/engine/score.hpp>

namespace dice
{
  enum class creature_size
  {
    fine,
    diminutive,
    tiny,
    small,
    medium,
    large,
    huge,
    gargantuan,
    colossal
  };

  struct game;

  struct DICE_API creature : engine::modifier_source
  {
    creature(const char* name);

    engine::ability strength;
    engine::ability dexterity;
    engine::ability constitution;
    engine::ability intelligence;
    engine::ability wisdom;
    engine::ability charisma;

    engine::score melee_attack_roll;
    engine::score ranged_attack_roll;
    engine::score melee_damage_roll;
    engine::score fortitude_saving_throw;
    engine::score reflex_saving_throw;
    engine::score will_saving_throw;
    engine::score hit_die;
    engine::score max_load;
    engine::score armor_class;
    engine::score touched_armor_class;
    engine::score speed;

    engine::last_one<creature_size, creature_size::medium> size;

    void attach_to(game& g, engine::linker& l) const;

    string name;
  };

  template <typename F>
  struct DICE_API ability_scores : engine::modifier_source
  {

    ability_scores(F f) : f_(f)
    {
    }

    void attach_to(creature& c, engine::linker& l) const
    {
      l.connect_value(c.strength, build_modifier());
      l.connect_value(c.dexterity, build_modifier());
      l.connect_value(c.constitution, build_modifier());
      l.connect_value(c.intelligence, build_modifier());
      l.connect_value(c.wisdom, build_modifier());
      l.connect_value(c.charisma, build_modifier());
    }

  private:
    F f_;

    engine::modifier build_modifier() const
    {
      return {f_(), engine::modifier::type::base_value, m_object_id};
    }
  };

  template <typename F>
  ability_scores<F> make_ability_scores(F&& f)
  {
    return ability_scores<F>(std::forward<F>(f));
  }
}
