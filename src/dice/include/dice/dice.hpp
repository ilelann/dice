#include <dice/config.hpp>

namespace dice
{

  using dice_roll_t = ushort;

  dice_roll_t d6();

  dice_roll_t d10();

  dice_roll_t d20();

  dice_roll_t sum4d6b3();

  dice_roll_t sum2d6();

  dice_roll_t sum3d6();
}
