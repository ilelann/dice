#pragma once

#include <dice/config.hpp>
#include <dice/engine/ability.hpp>
#include <dice/engine/linker.hpp>
#include <dice/engine/modifier_source.hpp>

namespace dice
{
  struct creature;

  DICE_API engine::ability::slot_type make_mod(const engine::ability&                  a,
                                               const engine::modifier_source::id_type& object_id);

  template <engine::ability creature::*src, engine::score creature::*dst>
  struct DICE_API ability_modifier : engine::modifier_source
  {
    void attach_to(creature& c, engine::linker& l) const
    {
      l.connect((c).*(dst), make_mod((c).*(src), m_object_id));
    }
  };
}
