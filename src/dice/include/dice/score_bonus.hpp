#pragma once

#include <dice/config.hpp>
#include <dice/engine/modifier.hpp>
#include <dice/engine/modifier_source.hpp>

namespace dice
{
  namespace engine
  {
    struct linker;
  }

  struct DICE_API score_bonus : engine::modifier_source
  {
    score_bonus(engine::modifier::result_type value, engine::modifier::type type);

    void attach_to(engine::score& c, engine::linker& l) const;

  private:
    engine::modifier::result_type value_m;
    engine::modifier::type        type_m;
  };
}
