#pragma once

#include <string>

namespace dice
{
  using integer  = int;
  using uinteger = unsigned int;
  using number   = double;
  using ushort   = unsigned short;

  using std::string;
}

#define DICE_API __attribute__((visibility("default")))
