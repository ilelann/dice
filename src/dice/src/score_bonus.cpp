#include <dice/engine/linker.hpp>
#include <dice/engine/score.hpp>
#include <dice/score_bonus.hpp>

namespace dice
{
  score_bonus::score_bonus(engine::score::result_type value, engine::modifier::type type)
      : value_m(value), type_m(type)
  {
  }

  void score_bonus::attach_to(engine::score& c, engine::linker& l) const
  {
    l.connect_value(c, engine::modifier{value_m, type_m, m_object_id});
  }
}
