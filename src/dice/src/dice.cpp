#include <dice/dice.hpp>

#include <algorithm>
#include <array>
#include <random>

namespace dice
{

  std::default_random_engine& re()
  {
    static std::default_random_engine e;

    return e;
  }

  template <dice_roll_t sides>
  dice_roll_t           roll()
  {
    static std::uniform_int_distribution<dice_roll_t> d{1, sides};

    return d(re());
  }

  dice_roll_t d6()
  {
    return roll<6>();
  }

  dice_roll_t d10()
  {
    return roll<10>();
  }

  dice_roll_t d20()
  {
    return roll<20>();
  }

  template <dice_roll_t X, dice_roll_t Y, dice_roll_t Z>
  dice_roll_t sumXdYbZ()
  {
    static_assert(Z <= X, "Cannot sum more rolls than thrown");

    std::array<dice_roll_t, X> results;
    std::generate(begin(results), end(results), roll<Y>);
    std::sort(begin(results), end(results), std::greater<dice_roll_t>());

    return std::accumulate(begin(results), begin(results) + Z, 0);
  }

  dice_roll_t sum4d6b3()
  {
    return sumXdYbZ<4, 6, 3>();
  }

  dice_roll_t sum2d6()
  {
    return d6() + d6();
  }

  dice_roll_t sum3d6()
  {
    return d6() + d6() + d6();
  }
}
