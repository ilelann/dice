#include <dice/ability_modifier.hpp>
#include <dice/engine/ability.hpp>

namespace dice
{
  DICE_API
  engine::ability::slot_type make_mod(const engine::ability&                  a,
                                      const engine::modifier_source::id_type& object_id)
  {
    return [&] {
      return engine::modifier{engine::score::result_type(a() / 2) - 5,
                              engine::modifier::type::ability_modifier,
                              object_id};
    };
  }
}
