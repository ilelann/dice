#include <dice/creature.hpp>
#include <dice/max_load_from_strength.hpp>

namespace dice
{
  namespace
  {
    engine::ability::result_type compute(const engine::ability::result_type& strength)
    {
      if (strength < 1) {
        throw std::runtime_error("invalid strength to evaluate max load");
      }

      switch (strength) {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10: return 100;
        case 11: return 115;
        case 12: return 130;
        case 13: return 150;
        case 14: return 175;
        case 15: return 200;
        case 16: return 230;
        case 17: return 260;
        case 18: return 300;
        case 19: return 350;
        case 20: return 400;
        case 21: return 460;
        case 22: return 520;
        case 23: return 600;
        case 24: return 700;
        case 25: return 800;
        case 26: return 920;
        case 27: return 1040;
        case 28: return 1200;
        case 29: return 1400;
        default: return 4 * compute(strength - 10);
      }
    }
  }

  void max_load_from_strength::attach_to(creature& c, engine::linker& l) const
  {
    l.connect(c.max_load, [this, &c] {
      return engine::modifier{compute(c.strength()), engine::modifier::type::base_value, c.id()};
    });
  }
}
