#include <dice/engine/identified.hpp>

namespace dice
{
  namespace engine
  {
    identified::id_type identified::counter = 0;

    identified::identified() : m_object_id{++counter}
    {
    }
  }
}
