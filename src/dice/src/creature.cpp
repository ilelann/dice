#include <dice/creature.hpp>
#include <dice/dice.hpp>
#include <dice/game.hpp>

namespace dice
{
  creature::creature(const char* a_name) : name(a_name)
  {
  }

  void creature::attach_to(game& g, engine::linker& l) const
  {
    l.connect_value(g.creatures, *this);
  }

  std::string name(const creature* c)
  {
    return c->name;
  }

  //  engine::score::result_type ability_scores::get_score() const
  //  {
  //    return 9;
  //  }

  //  engine::score::result_type standard_ability_scores::get_score() const
  //  {
  //    return sum4d6b3();
  //  }

  //  engine::score::result_type classic_ability_scores::get_score() const
  //  {
  //    return sum3d6();
  //  }

  //  engine::score::result_type heroic_ability_scores::get_score() const
  //  {
  //    return sum2d6() + 6;
  //  }
}
