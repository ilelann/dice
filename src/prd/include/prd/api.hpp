#pragma once

#include <dice/dice.hpp>
#include <prd/ext.hpp>

namespace prd
{

  namespace scores
  {
    const auto mean_ability_scores     = dice::make_ability_scores([] { return 9; });
    const auto standard_ability_scores = dice::make_ability_scores(dice::sum4d6b3);
    const auto classic_ability_scores  = dice::make_ability_scores(dice::sum3d6);
    const auto heroic_ability_scores = dice::make_ability_scores([] { return dice::sum2d6() + 6; });
  }

  constexpr Creature<HitDie<ConstitutionModifier>> Character;

  namespace races
  {
    constexpr Race<Constitution<Value<2>>,
                   Wisdom<Value<2>>,
                   Charisma<Value<-2>>,
                   Size<creature_size::medium>>
        Dwarf;

    constexpr Race<Charisma<Value<2>>
                   // Wisdom <2>,
                   //
                   // Speed <30>
                   //
                   // Senses
                   // {
                   //   Darkvision {
                   //     60ft
                   //   },
                   // },
                   // Diplomacy 2,
                   // Perception 2,
                   // SpellLikeAbilities
                   // {
                   //   Daylight "1/day"
                   // },
                   // "Celestial Resistance",
                   // {
                   //   acid 5,
                   //   cold 5,
                   //   electricity 5
                   // },
                   // Languages
                   // {
                   //   Celestial,
                   //   Common
                   // }
                   >
        Aasimar;
  }
}
