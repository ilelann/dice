#pragma once

#include <dice/api.hpp>
#include <dice/engine/object_of.hpp>

namespace prd
{
  namespace ext
  {
    using namespace dice;

    template <typename T, engine::score creature::*target, engine::modifier::type mod_type>
    struct score_bonus
    {
      template <typename Self>
      static auto attach_to(creature& c, engine::linker& l)
      {
        return T::template connect_to<Self>(c, ((c).*target), l, mod_type);
      }
    };

    template <int value>
    struct Value
    {
      template <typename Self>
      static auto connect_to(creature&,
                             engine::score&         target,
                             engine::linker&        l,
                             engine::modifier::type mod_type)
      {
        return l.connect_value(target, engine::modifier{value, mod_type, Self::id()});
      }
    };

    template <engine::ability creature::*source>
    struct AbilityModifier
    {
      template <typename Self>
      static auto
      connect_to(creature& c, engine::score& target, engine::linker& l, engine::modifier::type)
      {
        auto make_mod = [&] {
          return engine::modifier{engine::score::result_type(((c).*(source))() / 2) - 5,
                                  engine::modifier::type::ability_modifier,
                                  Self::id()};
        };

        return l.connect(target, make_mod);
      }
    };

    template <creature_size value>
    struct static_size_bonus
    {
      template <typename Self>
      static auto attach_to(creature& c, engine::linker& l)
      {
        return l.connect_value(c.size, value);
      }
    };

    template <engine::modifier::type mod_type, typename T>
    struct none_modifier_type_overrider
    {
      using type = T;
    };

    template <typename T, engine::modifier::type mod_type, engine::score creature::*target>
    struct none_modifier_type_overrider<mod_type,
                                        score_bonus<T, target, engine::modifier::type::none>>
    {
      using type = score_bonus<T, target, mod_type>;
    };

    template <engine::modifier::type mod_type, typename Self, typename other, typename... Args>
    struct none_modifier_type_overrider<mod_type, engine::compound_object_of<Self, other, Args...>>
    {
      using type =
          engine::compound_object_of<other, none_modifier_type_overrider<mod_type, Args>...>;
    };

    template <engine::score creature::*target, typename Self, typename... Args>
    using AlibityModifier = engine::compound_object_of<Self, creature, Args...>;
  }

  using ext::Value;

  template <typename T>
  using Strength =
      ext::score_bonus<T, &dice::creature::strength, dice::engine::modifier::type::none>;

  using StrengthModifier = ext::AbilityModifier<&dice::creature::strength>;

  template <typename T>
  using Dexterity =
      ext::score_bonus<T, &dice::creature::dexterity, dice::engine::modifier::type::none>;

  using DexterityModifier = ext::AbilityModifier<&dice::creature::dexterity>;

  template <typename T>
  using Constitution =
      ext::score_bonus<T, &dice::creature::constitution, dice::engine::modifier::type::none>;

  using ConstitutionModifier = ext::AbilityModifier<&dice::creature::constitution>;

  template <typename T>
  using Intelligence =
      ext::score_bonus<T, &dice::creature::intelligence, dice::engine::modifier::type::none>;

  using IntelligenceModifier = ext::AbilityModifier<&dice::creature::intelligence>;

  template <typename T>
  using Wisdom = ext::score_bonus<T, &dice::creature::wisdom, dice::engine::modifier::type::none>;

  using WisdomModifier = ext::AbilityModifier<&dice::creature::wisdom>;

  template <typename T>
  using Charisma =
      ext::score_bonus<T, &dice::creature::charisma, dice::engine::modifier::type::none>;

  using CharismaModifier = ext::AbilityModifier<&dice::creature::charisma>;

  template <typename T>
  using Speed = ext::score_bonus<T, &dice::creature::speed, dice::engine::modifier::type::none>;

  template <typename T>
  using HitDie = ext::score_bonus<T, &dice::creature::hit_die, dice::engine::modifier::type::none>;

  using ext::creature_size;
  template <creature_size value>
  using Size = ext::static_size_bonus<value>;

  template <dice::engine::modifier::type mod_type, typename... Args>
  struct overrided_compound
      : dice::engine::compound_object_of<overrided_compound<mod_type, Args...>,
                                         dice::creature,
                                         typename ext::none_modifier_type_overrider<mod_type,
                                                                                    Args>::type...>
  {
  };

  template <typename... Args>
  using Creature = overrided_compound<dice::engine::modifier::type::base_value, Args...>;

  template <typename... Args>
  using Race = overrided_compound<dice::engine::modifier::type::racial_bonus, Args...>;
}
